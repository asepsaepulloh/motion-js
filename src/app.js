import MojsPlayer from 'mojs-player'
import mojs from 'mo-js'

var shape = new mojs.Shape({
    shape: 'circle',
    top: "50%",
    left: "40%",
    radius: 25,
    fill: "white",
    stroke: "#e55252",
    strokeWidth: {0:8},
    strokeDasharray: "100%",
    strokeDashoffset: {"-100%": "100%"},
    duration:      3000,
    delay:         1000,
    easing:        'cubic.out',
    repeat:        999,
    isShowStart:  true
}).play()

var rect = new mojs.Shape({
    shape: 'rect',
    top: "50%",
    left: "50%",
    radius: 25,
    fill : "white",
    stroke: "#c300ff",
    strokeWidth: {0:8},
    strokeDasharray: "100%",
    strokeDashoffset: {"-100%": "100%"},
    duration:      3000,
    delay:         1000,
    easing:        'cubic.out',
    repeat:        999,
    isShowStart: true
}).play()

var polygon = new mojs.Shape({
    shape: 'polygon',
    scale: {0:1},
    top: "50%",
    left: "60%",
    strokeWidth: 8,
    radius: 35,
    fill : "white",
    stroke: "#b6dd40",
    delay:         1000,
    easing:        'cubic.out',
    duration:      3000
  }).then({
    strokeWidth: {0:8},
    strokeDasharray: "100%",
    strokeDashoffset: {"-100%": "100%"},
    duration:      3000,
    delay:         1000,
    easing:        'cubic.out',
    repeat:        999,
  }).play()
